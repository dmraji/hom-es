/*
  # David Raji's 2D matrix class

  - Highly portable
  - Scalable
  - Easily extendable
  - Intuitive syntax
  - Eliminates bulky dependencies like Eigen!

  ## Summary:
  The impotus for the development of this class was the apparent shortage of a simple, header-only CPP matrix library.
  Originally, the target use case was embedded development for ARM Cortex-M, but the resultant class has no problems scaling up.

  ## Technical Details
  Relies solely on STL, c++11 or greater. The matrices that this class produces are wrappers for std::vectors of std::vectors.
*/

#ifndef MATRIX_H
#define MATRIX_H

#include "application.h"

#include <vector>
#include <string>
#include <cmath>
// #include <utility>
// #include <cassert>

#include "lgamma_table.h"

#include "prandom.h"

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

template<typename T> class matrix
{
  private:
    std::vector< std::vector<T> > mat;
    uint32_t n_rows;
    uint32_t n_cols;

  public:

    // Initial value constructor
    matrix(const uint32_t _rows = 1,
           const uint32_t _cols = 1,
           const T & init_val = 0.0
           ) : n_rows(_rows), n_cols(_cols)
    {
      mat.resize(_rows, std::vector<T>(_cols, init_val));
    }

    // Copy constructor
    matrix(const matrix<T> & other)
    {
      mat = other.mat;
      n_rows = other.n_rows;
      n_cols = other.n_cols;
    }

    // Move constructor
    matrix(matrix<T> && other
           ) noexcept : mat(std::move(other.mat)),
                        n_rows(0),
                        n_cols(0)
    {
      std::swap(n_rows, other.n_rows);
      std::swap(n_cols, other.n_cols);
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Special value constructor
    matrix(const uint32_t _rows,
           const uint32_t _cols,
           const std::string initial_designator
           ) : n_rows(_rows), n_cols(_cols)
    {
      mat.resize(_rows, std::vector<T>(_cols, 0.0));

      prandom gen;

      if(!initial_designator.compare("ri"))
      {
        // Random integer initialization

        for(uint32_t i = 0; i < n_rows; ++i)
        {
          for(uint32_t j = 0; j < n_cols; ++j)
          {
            this->mat[i][j] = gen.random_i();
          }
        }
      }
      else if(!initial_designator.compare("rf"))
      {
        // Random float initialization

        for(uint32_t i = 0; i < n_rows; ++i)
        {
          for(uint32_t j = 0; j < n_cols; ++j)
          {
            this->mat[i][j] = gen.random_f();
          }
          // Serial.println(this->mat[i][0]);
        }
      }
      else
      {
        // Default: random float initialization

        for(uint32_t i = 0; i < n_rows; ++i)
        {
          for(uint32_t j = 0; j < n_cols; ++j)
          {
            this->mat[i][j] = gen.random_f();
          }
        }
      }
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Constructor to map from raw array
    // template<uint32_t N>
    matrix(const uint32_t _rows,
           const uint32_t _cols,
           const float arr[]
           ) : n_rows(_rows), n_cols(_cols)
    {
      mat.resize(_rows, std::vector<T>(_cols, 0.0));

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          this->mat[i][j] = arr[(i * _cols) + j];
          // Serial.println(this->mat[i][j], 6);
        }
      }
    }

    // Vector mapping constructor
    matrix(uint32_t _rows,
           uint32_t _cols,
           std::vector<float> & vec
           ) : n_rows(_rows), n_cols(_cols)
    {
      mat.resize(_rows, std::vector<T>(_cols, 0.0));

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          this->mat[i][j] = vec[(i * _cols) + j];
        }
      }
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Default destructor
    ~matrix() {}

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Transposition of matrix
    matrix<T> transpose() const
    {
      matrix<T> result(n_cols, n_rows, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(j, i) = this->mat[i][j];
        }
      }

      return result;
    }

    // Slice up matrix
    matrix<T> slice(uint32_t r_slice,
                    uint32_t c_slice
                    ) const
    {
      matrix<T> result(r_slice, c_slice, 0.0);

      if((r_slice > n_rows) || (c_slice > n_cols))
      {
        Serial.println("Invalid slice index!");
        delay(1000);
      }
      // assert(r_slice < n_rows);
      // assert(c_slice < n_cols);

      for(uint32_t i = 0; i < r_slice; ++i)
      {
        for(uint32_t j = 0; j < c_slice; ++j)
        {
          result(i, j) = this->mat[i][j];
        }
      }
    }

    // Replicate portion of matrix
    matrix<T> replicate(uint32_t r_rows,
                        uint32_t r_cols
                        ) const
    {
      matrix<T> result(n_rows * r_rows, n_cols * r_cols, 0.0);

      for(uint32_t i = 0; i < result.get_rows(); ++i)
      {
        for(uint32_t j = 0; j < result.get_cols(); ++j)
        {
          result(i, j) = this->mat[i % n_rows][j % n_cols];
        }
      }

      return result;
    }

    // Resize matrix
    // NOTE: new matrix filled with zeros if extended beyond original dimensions
    matrix<T> resize(uint16_t r_rows,
                     uint16_t r_cols
                     ) const
    {
      matrix<T> result(r_rows, r_cols, 0.0);

      for(uint16_t i = 0; i < r_rows; ++i)
      {
        for(uint16_t j = 0; j < r_cols; ++j)
        {
          if( (i > (n_rows - 1) ) || (j > (n_cols - 1) ) )
          {
            result(i, j) = 0.0;
          }
          else
          {
            result(i, j) = this->mat[i][j];
          }
        }
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Fetch single row of matrix
    matrix<T> s_row(const uint32_t row_index)
    {
      matrix<T> result(1, n_cols, 0.0);

      for(uint32_t j = 0; j < n_cols; ++j)
      {
        result(0, j) = this->mat[row_index][j];
      }
      return result;
    }

    // Fetch single column of matrix
    matrix<T> s_col(const uint32_t col_index)
    {
      matrix<T> result(n_rows, 1, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        result(i, 0) = this->mat[i][col_index];
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Fetch private size members
    uint32_t get_rows() const { return this->n_rows; }
    uint32_t get_cols() const { return this->n_cols; }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Assignment operator
    matrix<T> & operator= (const matrix<T> & other)
    {
      if(&other == this)
      {
        return *this;
      }

      uint32_t new_rows = other.get_rows();
      uint32_t new_cols = other.get_cols();

      mat.resize(new_rows);
      for(uint32_t i = 0; i < mat.size(); ++i)
      {
        mat[i].resize(new_cols);
      }

      for(uint32_t i = 0; i < new_rows; ++i)
      {
        for(uint32_t j = 0; j < new_cols; ++j)
        {
          mat[i][j] = other(i, j);
        }
      }

      n_rows = new_rows;
      n_cols = new_cols;

      return *this;
    }

    // Move assignment operator
    matrix<T> & operator= (matrix<T> && other) noexcept
    {
      if(this != &other)
      {
        std::swap(mat, other.mat);
        std::swap(n_rows, other.n_rows);
        std::swap(n_cols, other.n_cols);
      }

      return *this;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    /*
      Mathematical Operations
    */

    // Matrix + other matrix
    matrix<T> operator+ (const matrix<T> & other)
    {
      // Assert both sides have same dimensions
      // assert(n_rows == other.n_rows);
      // assert(n_cols == other.n_cols);
      if(!(n_rows == other.n_rows) || !(n_cols == other.n_cols))
      {
        Serial.println("Matrix shape mismatch!");
        delay(1000);
      }

      matrix result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] + other(i, j);
        }
      }

      return result;
    }

    // Matrix + scalar
    matrix<T> operator+ (const T& scalar_other) const
    {
      matrix result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] + scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Matrix - other matrix
    matrix<T> operator- (const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      // assert(n_rows == other.n_rows);
      // assert(n_cols == other.n_cols);
      if(!(n_rows == other.n_rows) || !(n_cols == other.n_cols))
      {
        Serial.println("Matrix shape mismatch!");
        delay(1000);
      }

      matrix result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] - other(i, j);
        }
      }

      return result;
    }

    // Matrix - scalar
    matrix<T> operator- (const T & scalar_other) const
    {
      matrix result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] - scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Matrix * other matrix
    matrix<T> operator* (const matrix<T> & other) const
    {
      uint32_t row_major = this->n_rows;
      uint32_t col_major = other.get_cols();
      uint32_t c_minor = this->n_cols;
      uint32_t r_minor= other.get_rows();

      // Assert minors equivalent dimensions
      // assert(c_minor == r_minor);
      if(!(c_minor == r_minor))
      {
        Serial.println("Matrix minor dimension mismatch! ( this->mat.get_cols() != other_mat.get_rows() )");
        delay(1000);
      }

      matrix<T> result(row_major, col_major, 0.0);

      T tmp;
      for(uint32_t i = 0; i < row_major; ++i)
      {
        for(uint32_t j = 0; j < col_major; ++j)
        {
          tmp = 0;
          for(uint32_t p = 0; p < c_minor; ++p)
          {
            tmp = tmp + (this->mat[i][p] * other(p, j));
          }
          result(i, j) = tmp;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cwise product
    matrix<T> cwise_prod(const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      // assert(n_rows == other.n_rows);
      // assert(n_cols == other.n_cols);
      if(!(n_rows == other.n_rows) || !(n_cols == other.n_cols))
      {
        Serial.println("Matrix shape mismatch!");
        delay(1000);
      }

      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] * other(i, j);
        }
      }

      return result;
    }

    // Cwise product with scalar
    matrix<T> cwise_prod(const T & scalar_other) const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] * scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cwise quotient
    matrix<T> cwise_quot(const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      // assert(n_rows == other.n_rows);
      // assert(n_cols == other.n_cols);
      if(!(n_rows == other.n_rows) || !(n_cols == other.n_cols))
      {
        Serial.println("Matrix shape mismatch!");
        delay(1000);
      }

      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] / other(i, j);
        }
      }

      return result;
    }

    // Cwise quotient with scalar
    matrix<T> cwise_quot(const T & scalar_other) const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] / scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Natural logarithm (cwise)
    matrix<T> m_log() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          // Prevent undefined from log(0)
          // assert(this->mat[i][j] != 0.0);
          // if(this->mat[i][j] == 0.0)
          // {
          //   Serial.println("Zero in matrix given .m_log()! Expect undefined behavior!");
          //   delay(1000);
          // }

          result(i, j) = log(this->mat[i][j]);
        }
      }

      return result;
    }

    // Natural logarithmic gamma (cwise)
    matrix<T> m_lgamma() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          // Prevent undefined from log(0)
          // assert(this->mat[i][j] != 0.0);
          // if(this->mat[i][j] == 0.0)
          // {
          //   Serial.println("Zero in matrix given .m_lgamma()! Expect undefined behavior!");
          //   delay(1000);
          // }

          result(i, j) = lgamma(this->mat[i][j]);
          // result(i, j) = lgt.vals[(int)this->mat[i][j]];
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // matrix<float> m_abs() const
    // {
    //   matrix<T> result(n_rows, n_cols, 0.0);
    //
    //   for(uint32_t i = 0; i < n_rows; ++i)
    //   {
    //     for(uint32_t j = 0; j < n_cols; ++j)
    //     {
    //       // copy and re-interpret as 32 bit integer
    //       int casted = *(int*) &this->mat[i][j];
    //       // clear highest bit
    //       casted &= 0x7FFFFFFF;
    //       // re-interpret as float
    //       result(i, j) = *(float*)&casted;
    //     }
    //   }
    //
    //   return result;
    // }

    matrix<T> m_abs() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = std::fabs(this->mat[i][j]);
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum of matrix elements
    T sum() const
    {
      T result = 0.0;

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          result += this->mat[i][j];
        }
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum along rows
    matrix<T> rowwise_sum() const
    {
      matrix<T> result(n_rows, 1);

      T tmp;
      for(uint32_t i = 0; i < n_rows; ++i)
      {
        tmp = 0;
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          tmp += this->mat[i][j];
        }
        result(i, 0) = tmp;
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum along columns
    matrix<T> colwise_sum() const
    {
      matrix<T> result(1, n_cols);

      T tmp;
      for(uint32_t i = 0; i < n_cols; ++i)
      {
        tmp = 0;
        for(uint32_t j = 0; j < n_rows; ++j)
        {
          tmp += this->mat[j][i];
        }
        result(0, i) = tmp;
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cumulative sum of elements
    std::vector<T> cumsum() const
    {
      std::vector<T> result(n_rows * n_cols, 0.0);

      T csum = 0.0;

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          csum += this->mat[i][j];
          result[(i * n_cols) + j] = csum;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Calculate mean of matrix elements
    T mean() const
    {
      return this->sum() / (n_cols * n_rows);
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Check that all elements are real
    bool is_real() const
    {
      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          if(this->mat[i][j] <= 0)
          {
            return FALSE;
          }
        }
      }

      return TRUE;
    }

    // Enforce real numbers for all elements using an epsilon
    void realize(float EPS = 1E-6f)
    {
      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          if(this->mat[i][j] <= 0)
          {
            this->mat[i][j] = EPS;
          }
        }
      }
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Fetch maximum element in matrix
    T max() const
    {
      // Initialize to first element
      T _max = this->mat[0][0];

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          if(this->mat[i][j] > _max)
          {
            _max = this->mat[i][j];
          }
        }
      }
      return _max;
    }

    // Fetch minimum element in matrix
    T min() const
    {
      // Initialize to first element
      T _min = this->mat[0][0];

      for(uint32_t i = 0; i < n_rows; ++i)
      {
        for(uint32_t j = 0; j < n_cols; ++j)
        {
          if(this->mat[i][j] < _min)
          {
            _min = this->mat[i][j];
          }
        }
      }
      return _min;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Access the individual elements
    T& operator() (const uint32_t & row,
                   const uint32_t & col
                   )
    {
      return this->mat[row][col];
    }

    // Access the individual elements (const)
    const T& operator() (const uint32_t & row,
                         const uint32_t & col
                         ) const
    {
      return this->mat[row][col];
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
};

#endif
