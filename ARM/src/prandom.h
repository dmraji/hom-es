#ifndef PRANDOM_H
#define PRANDOM_H

// #include <cstdint>

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

struct prandom
{
  prandom()
  {
    uint8_t seed = 19;
    srand(seed);
  }

  ~prandom() {};

  //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

  uint16_t random_i(uint16_t max = 100)
  {
    return random_i(0, max);
  }

  uint16_t random_i(uint16_t min,
                    uint16_t max
                    )
  {
    return rand() % (max - min + 1) + min;
  }

  float random_f(uint16_t max = 1,
                 uint32_t shift = 1E3
                 )
  {
    return random_f(0, max, shift);
  }

  float random_f(uint16_t min,
                 uint16_t max,
                 uint32_t shift
                 )
  {
    uint32_t smax = shift * max;
    return float(rand() % (smax - min + 1) + min) / float(shift);
  }

  //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

};

#endif
