# Header Only Matrices - Embedded/Scalable (HOM-ES)

- Highly portable
- Scalable
- Easily extendable
- Intuitive syntax
- Eliminates bulky dependencies like Eigen!

## Summary:
The impetus for the development of this class was the apparent shortage of a simple, header-only CPP matrix library.

Originally, the target use case was embedded development for ARM Cortex-M, but the resultant class has no problems scaling up.

## Technical Details:
Relies solely on STL, c++11 or greater. The matrices that this class produces are wrappers for std::vectors of std::vectors.
