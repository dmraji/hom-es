#include "matrix.h"

/*
  Matrix Class Member Functions
*/

// Initial value constructor
template<typename T>
matrix<T>::matrix(uint16_t _rows,
       uint16_t _cols,
       const T & init_val
       ) : n_rows(_rows), n_cols(_cols)
{
  mat.resize(_rows, std::vector<T>(_cols, init_val));
}

// Copy constructor
template<typename T>
matrix<T>::matrix(const matrix<T> & other)
{
  mat = other.mat;
  n_rows = other.n_rows;
  n_cols = other.n_cols;
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Move constructor
template<typename T>
matrix<T>::matrix(matrix<T> && other
       ) noexcept : mat(std::move(other.mat)),
                    n_rows(std::exchange(other.n_rows, 0)),
                    n_cols(std::exchange(other.n_cols, 0))
{}



//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Special value constructor
template<typename T>
matrix<T>::matrix(uint16_t _rows,
       uint16_t _cols,
       const std::string initial_designator
       ) : n_rows(_rows), n_cols(_cols)
{
  mat.resize(_rows, std::vector<T>(_cols, 0.0));

  prandom gen;

  if(!initial_designator.compare("ri"))
  {
    // Random integer initialization
    for(uint16_t i = 0; i < n_rows; ++i)
    {
      for(uint16_t j = 0; j < n_cols; ++j)
      {
        this->mat[i][j] = gen.random_i();
      }
    }
  }
  else if(!initial_designator.compare("rf"))
  {
    // Random float initialization
    for(uint16_t i = 0; i < n_rows; ++i)
    {
      for(uint16_t j = 0; j < n_cols; ++j)
      {
        this->mat[i][j] = gen.random_f();
      }
      // Serial.println(this->mat[i][0]);
    }
  }
  else
  {
    // Default: random float initialization
    for(uint16_t i = 0; i < n_rows; ++i)
    {
      for(uint16_t j = 0; j < n_cols; ++j)
      {
        this->mat[i][j] = gen.random_f();
      }
    }
  }
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Constructor to map from raw array
// template<uint16_t N>
template<typename T>
matrix<T>::matrix(uint16_t _rows,
       uint16_t _cols,
       float arr[]
       ) : n_rows(_rows), n_cols(_cols)
{
  mat.resize(_rows, std::vector<T>(_cols, 0.0));

  for(uint16_t i = 0; i < n_rows; ++i)
  {
    for(uint16_t j = 0; j < n_cols; ++j)
    {
      this->mat[i][j] = arr[(i * _cols) + j];
    }
  }
}

// Vector mapping constructor
template<typename T>
matrix<T>::matrix(uint16_t _rows,
       uint16_t _cols,
       std::vector<float> & vec
       ) : n_rows(_rows), n_cols(_cols)
{
  mat.resize(_rows, std::vector<T>(_cols, 0.0));

  for(uint16_t i = 0; i < n_rows; ++i)
  {
    for(uint16_t j = 0; j < n_cols; ++j)
    {
      this->mat[i][j] = vec[(i * _cols) + j];
    }
  }
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Default destructor
template<typename T>
matrix<T>::~matrix() {}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Transposition of matrix
template<typename T>
matrix<T> matrix<T>::transpose() const
{
  matrix<T> result(n_cols, n_rows, 0.0);

  for(uint16_t i = 0; i < n_rows; ++i)
  {
    for(uint16_t j = 0; j < n_cols; ++j)
    {
      result(j, i) = this->mat[i][j];
    }
  }

  return result;
}

// Slice up matrix
template<typename T>
matrix<T> matrix<T>::slice(uint16_t r_slice,
                uint16_t c_slice
                ) const
{
  matrix<T> result(r_slice, c_slice, 0.0);

  assert(r_slice < n_rows);
  assert(c_slice < n_cols);

  for(uint16_t i = 0; i < r_slice; ++i)
  {
    for(uint16_t j = 0; j < c_slice; ++j)
    {
      result(i, j) = this->mat[i][j];
    }
  }
  return result;
}

// Replicate portion of matrix
template<typename T>
matrix<T> matrix<T>::replicate(uint16_t r_rows,
                    uint16_t r_cols
                    ) const
{
  matrix<T> result(n_rows * r_rows, n_cols * r_cols, 0.0);

  for(uint16_t i = 0; i < result.get_rows(); ++i)
  {
    for(uint16_t j = 0; j < result.get_cols(); ++j)
    {
      result(i, j) = this->mat[i % n_rows][j % n_cols];
    }
  }
  return result;
}

// Resize matrix
// NOTE: new matrix filled with zeros if extended beyond original dimensions
template<typename T>
matrix<T> matrix<T>::resize(uint16_t r_rows,
                 uint16_t r_cols
                 ) const
{
  matrix<T> result(r_rows, r_cols, 0.0);

  for(uint16_t i = 0; i < r_rows; ++i)
  {
    for(uint16_t j = 0; j < r_cols; ++j)
    {
      if( (i > (n_rows - 1) ) || (j > (n_cols - 1) ) )
      {
        result(i, j) = 0.0;
      }
      else
      {
        result(i, j) = this->mat[i][j];
      }
    }
  }
  return result;
}

// Fetch single row of matrix
template<typename T>
matrix<T> matrix<T>::s_row(const uint32_t row_index) const
{
  matrix<T> result(1, n_cols, 0.0);

  for(uint32_t j = 0; j < n_cols; ++j)
  {
    result(0, j) = this->mat[row_index][j];
  }
  return result;
}

// Fetch single column of matrix
template<typename T>
matrix<T> matrix<T>::s_col(const uint32_t col_index) const
{
  matrix<T> result(n_rows, 1, 0.0);

  for(uint32_t i = 0; i < n_rows; ++i)
  {
    result(i, 0) = this->mat[i][col_index];
  }
  return result;
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Fetch private size members
template<typename T>
uint16_t matrix<T>::get_rows() const { return this->n_rows; }

template<typename T>
uint16_t matrix<T>::get_cols() const { return this->n_cols; }

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Assignment operator
template<typename T>
matrix<T> & matrix<T>::operator= (const matrix<T> & other)
{
  if(&other == this)
  {
    return *this;
  }

  uint16_t new_rows = other.get_rows();
  uint16_t new_cols = other.get_cols();

  mat.resize(new_rows);
  for(uint16_t i = 0; i < mat.size(); ++i)
  {
    mat[i].resize(new_cols);
  }

  for(uint16_t i = 0; i < new_rows; ++i)
  {
    for(uint16_t j = 0; j < new_cols; ++j)
    {
      mat[i][j] = other(i, j);
    }
  }
  n_rows = new_rows;
  n_cols = new_cols;

  return *this;
}

// Move assignment operator
template<typename T>
matrix<T> & matrix<T>::operator= (matrix<T> && other) noexcept
{
  if(this != &other)
  {
    std::swap(mat, other.mat);
    std::swap(n_rows, other.n_rows);
    std::swap(n_cols, other.n_cols);
  }

  return *this;
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Cumulative sum of elements
// NOTE: flattens input matrix row-major
template<typename T>
std::vector<T> matrix<T>::cumsum() const
{
  std::vector<T> result(n_rows * n_cols, 0.0);

  T csum = 0.0;

  for(uint32_t i = 0; i < n_rows; ++i)
  {
    for(uint32_t j = 0; j < n_cols; ++j)
    {
      csum += this->mat[i][j];
      result[(i * n_cols) + j] = csum;
    }
  }

  return result;
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Calculate mean of matrix elements
template<typename T>
T matrix<T>::mean() const
{
  return this->sum() / (n_cols * n_rows);
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Enforce real numbers for all elements
template<typename T>
void matrix<T>::realize()
{
  for(uint16_t i = 0; i < n_rows; ++i)
  {
    for(uint16_t j = 0; j < n_cols; ++j)
    {
      if(this->mat[i][j] <= 0)
      {
        this->mat[i][j] = 1E-6f;
      }
    }
  }
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Fetch maximum element in matrix
template<typename T>
T matrix<T>::max() const
{
  // Initialize to first element
  T _max = this->mat[0][0];

  for(uint32_t i = 0; i < n_rows; ++i)
  {
    for(uint32_t j = 0; j < n_cols; ++j)
    {
      if(this->mat[i][j] > _max)
      {
        _max = this->mat[i][j];
      }
    }
  }
  return _max;
}

// Fetch minimum element in matrix
template<typename T>
T matrix<T>::min() const
{
  // Initialize to first element
  T _min = this->mat[0][0];

  for(uint32_t i = 0; i < n_rows; ++i)
  {
    for(uint32_t j = 0; j < n_cols; ++j)
    {
      if(this->mat[i][j] > _min)
      {
        _min = this->mat[i][j];
      }
    }
  }
  return _min;
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// Print the matrix dimensions
template<typename T>
const void matrix<T>::print_dims() const
{
  std::cout << "(" << n_rows << ", " << n_cols << ")" << '\n';
}

//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

// OStream operator for easy CLI vis/writing to files
std::ostream& operator<< (std::ostream& os,
                          const matrix<float>& mat
                          )
{
  for(uint32_t i = 0; i < mat.get_rows(); ++i)
  {
    for(uint32_t j = 0; j < mat.get_cols(); ++j)
    {
      // std::cout << "mat(i, j) = " << mat(i, j) << '\n';
      os << mat(i, j) << ',' << ' ';
    }
    os << '\n';
  }
  return os;
}

// np.arange-like (row major)
template<typename T>
matrix<float> arange(const T range_begin,
                     const T range_end,
                     const T spacing = 1.0f
                     )
{
  uint32_t n_eles = (range_end - range_begin) / spacing;

  matrix<float> result(n_eles, 1, 0.0f);

  for(uint32_t i = 0; i < 1; ++i)
  {
    for(uint32_t j = 0; j < n_eles; ++j)
    {
      result(i, j) = range_begin + (i * spacing);
    }
  }

  return result;
}

// Explicit template instantiation
template class matrix<int>;
template class matrix<float>;
template class matrix<double>;
