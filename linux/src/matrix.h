/*
  # David Raji's 2D matrix class

  - Highly portable
  - Scalable
  - Easily extendable
  - Intuitive syntax
  - Eliminates bulky dependencies like Eigen!

  ## Summary:
  The impotus for the development of this class was the apparent shortage of a simple, header-only CPP matrix library.
  Originally, the target use case was embedded development for ARM Cortex-M, but the resultant class has no problems scaling up.

  ## Technical Details
  Relies solely on STL, c++11 or greater. The matrices that this class produces are wrappers for std::vectors of std::vectors.
*/

#ifndef MATRIX_H
#define MATRIX_H

#include <utility>
#include <vector>
#include <string>
#include <cmath>
#include <cassert>
#include <iostream>

#include "prandom.h"
#include "math_utils.h"

#include "lookup_tables.h"

// Log and lgamma lookup table
constexpr auto math_table = ltable<1000>();
// math_table.write();

template<typename T> class matrix
{
  private:
    std::vector< std::vector<T> > mat;
    uint16_t n_rows;
    uint16_t n_cols;

  public:

    // Initial value constructor
    matrix(uint16_t _rows = 1,
           uint16_t _cols = 1,
           const T & init_val = 0.0
         );

    // Copy constructor
    matrix(const matrix<T> & other);

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Move constructor
    matrix(matrix<T> && other) noexcept;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Special value constructor
    matrix(uint16_t _rows,
           uint16_t _cols,
           const std::string initial_designator
         );

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Constructor to map from raw array
    // template<uint16_t N>
    matrix(uint16_t _rows,
           uint16_t _cols,
           float arr[]
         );

    // Vector mapping constructor
    matrix(uint16_t _rows,
           uint16_t _cols,
           std::vector<float> & vec
         );

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Default destructor
    ~matrix();

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Transposition of matrix
    matrix<T> transpose() const;

    // Slice up matrix
    matrix<T> slice(uint16_t r_slice,
                    uint16_t c_slice
                  ) const;

    // Replicate portion of matrix
    matrix<T> replicate(uint16_t r_rows,
                        uint16_t r_cols
                      ) const;

    // Resize matrix
    // NOTE: new matrix filled with zeros if extended beyond original dimensions
    matrix<T> resize(uint16_t r_rows,
                     uint16_t r_cols
                   ) const;

    // Fetch single row of matrix
    matrix<T> s_row(const uint32_t row_index) const;

    // Fetch single column of matrix
    matrix<T> s_col(const uint32_t col_index) const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Fetch private size members
    uint16_t get_rows() const;
    uint16_t get_cols() const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Assignment operator
    matrix<T> & operator= (const matrix<T> & other);

    // Move assignment operator
    matrix<T> & operator= (matrix<T> && other) noexcept;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    /*
      Mathematical Operations
    */

    // Matrix + other matrix
    inline matrix<T> operator+ (const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      assert(n_rows == other.n_rows);
      assert(n_cols == other.n_cols);
      matrix result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] + other(i, j);
        }
      }
      return result;
    }

    // Matrix + scalar
    inline matrix<T> operator+ (const T& scalar_other) const
    {
      matrix result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] + scalar_other;
        }
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Matrix - other matrix
    inline matrix<T> operator- (const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      assert(n_rows == other.n_rows);
      assert(n_cols == other.n_cols);
      matrix result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] - other(i, j);
        }
      }
      return result;
    }

    // Matrix - scalar
    inline matrix<T> operator- (const T & scalar_other) const
    {
      matrix result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] - scalar_other;
        }
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Matrix * other matrix
    inline matrix<T> operator* (const matrix<T> & other) const
    {
      // Assert minors equivalent dimensions
      assert(this->n_cols == other.get_rows());

      uint16_t row_major = this->n_rows;
      uint16_t col_major = other.get_cols();

      matrix<T> result(row_major, col_major, 0.0);

      T tmp;
      for(uint16_t i = 0; i < row_major; ++i)
      {
        for(uint16_t j = 0; j < col_major; ++j)
        {
          tmp = 0;
          for(uint16_t p = 0; p < this->n_cols; ++p)
          {
            tmp += (this->mat[i][p] * other(p, j));
          }
          result(i, j) = tmp;
        }
      }
      return result;
    }

    // Matrix * scalar
    inline matrix<T> operator* (const T & scalar_other) const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] * scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Matrix / scalar
    inline matrix<T> operator/ (const T & scalar_other) const
    {
      // ...
      assert(scalar_other != 0.0);
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] / scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cwise product with other matrix
    inline matrix<T> cwise_prod(const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      assert(n_rows == other.n_rows);
      assert(n_cols == other.n_cols);

      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] * other(i, j);
        }
      }

      return result;
    }

    // Cwise product with scalar
    inline matrix<T> cwise_prod(const T & scalar_other) const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] * scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cwise quotient with other matrix
    inline matrix<T> cwise_quot(const matrix<T> & other) const
    {
      // Assert both sides have same dimensions
      assert(n_rows == other.n_rows);
      assert(n_cols == other.n_cols);

      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          // Sigh
          assert(other(i, j) != 0.0);
          result(i, j) = this->mat[i][j] / other(i, j);
        }
      }

      return result;
    }

    // Cwise quotient with scalar
    inline matrix<T> cwise_quot(const T & scalar_other) const
    {
      // Does this need explaining?
      assert(scalar_other != 0.0);
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = this->mat[i][j] / scalar_other;
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Natural logarithm (cwise)
    inline matrix<T> m_log() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          // Prevent undefined from log(0)
          assert(this->mat[i][j] != 0.0);

          // std::cout << this->mat[i][j] << '\n';
          result(i, j) = log(this->mat[i][j]);
          // result(i, j) = fast_log(this->mat[i][j]);
          // result(i, j) = math_table.values[ (int)this->mat[i][j] ][1];
        }
      }

      return result;
    }

    // Logarithmic gamma function (cwise)
    inline matrix<T> m_lgamma() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          // Prevent undefined from lgamma(0)
          assert(this->mat[i][j] != 0.0);

          // std::cout << this->mat[i][j] << '\n';
          result(i, j) = lgamma(this->mat[i][j]);
          // result(i, j) = fast_lgamma(this->mat[i][j]);
          // result(i, j) = math_table.values[ (int)this->mat[i][j] ][2];
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Absolute value of matrix (cwise)
    inline matrix<T> m_abs() const
    {
      matrix<T> result(n_rows, n_cols, 0.0);

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result(i, j) = std::fabs(this->mat[i][j]);
        }
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum of matrix elements
    inline T sum() const
    {
      T result = 0.0;

      for(uint16_t i = 0; i < n_rows; ++i)
      {
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          result += this->mat[i][j];
        }
      }
      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum along rows
    inline matrix<T> rowwise_sum() const
    {
      matrix<T> result(n_rows, 1);

      T tmp;
      for(uint16_t i = 0; i < n_rows; ++i)
      {
        tmp = 0;
        for(uint16_t j = 0; j < n_cols; ++j)
        {
          tmp += this->mat[i][j];
        }
        result(i, 0) = tmp;
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Sum along columns
    inline matrix<T> colwise_sum() const
    {
      matrix<T> result(1, n_cols);

      T tmp;
      for(uint16_t i = 0; i < n_cols; ++i)
      {
        tmp = 0;
        for(uint16_t j = 0; j < n_rows; ++j)
        {
          tmp += this->mat[j][i];
        }
        result(0, i) = tmp;
      }

      return result;
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Cumulative sum of elements
    // NOTE: flattens input matrix row-major
    std::vector<T> cumsum() const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Calculate mean of matrix elements
    T mean() const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Enforce real numbers for all elements
    void realize();

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Fetch maximum element in matrix
    T max() const;

    // Fetch minimum element in matrix
    T min() const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Access the individual elements
    T& operator() (const int& row,
                   const int& col
                   )
    {
      return this->mat[row][col];
    }

    // Access the individual elements (const)
    const T& operator() (const int& row,
                         const int& col
                         ) const
    {
      return this->mat[row][col];
    }

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__

    // Print the matrix dimensions
    const void print_dims() const;

    //__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__//__
};

std::ostream& operator<< (std::ostream& os,
                          const matrix<float>& mat
                          );

#endif
