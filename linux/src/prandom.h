#ifndef PRANDOM_H
#define PRANDOM_H

struct prandom
{
  prandom()
  {
    uint32_t seed = 19;
    srand(seed);
  }

  int random_i(int max = 100)
  {
    return random_i(0, max);
  }

  int random_i(int min,
               int max
               )
  {
    return rand() % (max - min + 1) + min;
  }

  float random_f(int max = 1,
                 int shift = 1e3
                 )
  {
    return random_f(0, max, shift);
  }

  float random_f(int min,
                 int max,
                 int shift
                 )
  {
    int smax = shift * max;
    return float(rand() % (smax - min + 1) + min) / float(shift);
  }

  ~prandom() {};
};

#endif
